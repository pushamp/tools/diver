# Diver
A utility for generating manifests based on data from external storages with the ability to use your own manifests to get a ready-made deployment configuration to a Kubernetes cluster

Supported storage

* Environment variable

The latest version: [1.1.0](https://gitlab.com/pushamp/tools/diver/-/package_files/10705172/download)
## Использование
```diver [SUBCOMMAND]```

[Функционал для генерации полного набора манифестов для onestep деплоя](/docs/Diver.md)



------------
    
    docker run -v $PWD:/volume --rm -it clux/muslrust /volume/build_rust.sh

