use snafu::Snafu;
use std::collections::HashMap;
use std::env;
use tera::Context;

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("Unable to read configuration from {}: {}", in_string, source))]
    JSONError {
        source: serde_json::Error,
        in_string: String,
    },

    #[snafu(display("Unable to write result to {}: {}", in_string, source))]
    YAMLError {
        source: serde_yaml::Error,
        in_string: String,
    },
}

type Result<T, E = Error> = std::result::Result<T, E>;

pub struct Environment;

pub trait Backend {
    fn get_context(&self) -> Result<Context> {
        self._get_context()
    }

    fn _get_context(&self) -> Result<Context>;
}

impl Backend for Environment {
    fn _get_context(&self) -> Result<Context> {
        let envs: HashMap<_, _> = env::vars().collect();
        let mut context = Context::new();
        context.insert("envs", &envs);
        Ok(context)
    }
}

impl Environment {
    pub fn get_by_keys(keys: &[String]) -> HashMap<String, String> {
        let mut envs: HashMap<_, _> = env::vars().collect();
        envs.retain(|k, _v| keys.contains(k));
        envs
    }
}
